package helpers

import "fmt"

func GetTCDCByYear(year int) string {
	check := year % 60
	var tcdc string
	switch check {
	case 0:
		tcdc = "Canh Thân"
		break
	case 1:
		tcdc = "Tân Dậu"
		break
	case 2:
		tcdc = "Nhâm Tuất"
		break
	case 3:
		tcdc = "Quý Hợi"
		break
	case 4:
		tcdc = "Giáp Tý"
		break
	case 5:
		tcdc = "Ất Sửu"
		break
	case 6:
		tcdc = "Bính Dần"
		break
	case 7:
		tcdc = "Đinh Mão"
		break
	case 8:
		tcdc = "Mậu Thìn"
		break
	case 9:
		tcdc = "Kỷ Tỵ"
		break
	case 10:
		tcdc = "Canh Ngọ"
		break
	case 11:
		tcdc = "Tân Mùi"
		break
	case 12:
		tcdc = "Nhâm Thân"
		break
	case 13:
		tcdc = "Quý Dậu"
		break
	case 14:
		tcdc = "Giáp Tuất"
		break
	case 15:
		tcdc = "Ất Hợi"
		break
	case 16:
		tcdc = "Bính Tý"
		break
	case 17:
		tcdc = "Đinh Sửu"
		break
	case 18:
		tcdc = "Mậu Dần"
		break
	case 19:
		tcdc = "Kỷ Mão"
		break
	case 20:
		tcdc = "Canh Thìn"
		break
	case 21:
		tcdc = "Tân Tỵ"
		break
	case 22:
		tcdc = "Nhâm Ngọ"
		break
	case 23:
		tcdc = "Quý Mùi"
		break
	case 24:
		tcdc = "Giáp Thân"
		break
	case 25:
		tcdc = "Ất Dậu"
		break
	case 26:
		tcdc = "Bính Tuất"
		break
	case 27:
		tcdc = "Đinh Hợi"
		break
	case 28:
		tcdc = "Mậu Tý"
		break
	case 29:
		tcdc = "Kỷ Sửu"
		break
	case 30:
		tcdc = "Canh Dần"
		break
	case 31:
		tcdc = "Tân Mão"
		break
	case 32:
		tcdc = "Nhâm Thìn"
		break
	case 33:
		tcdc = "Quý Tỵ"
		break
	case 34:
		tcdc = "Giáp Ngọ"
		break
	case 35:
		tcdc = "Ất Mùi"
		break
	case 36:
		tcdc = "Bính Thân"
		break
	case 37:
		tcdc = "Đinh Dậu"
		break
	case 38:
		tcdc = "Mậu Tuất"
		break
	case 39:
		tcdc = "Kỷ Hợi"
		break
	case 40:
		tcdc = "Canh Tý"
		break
	case 41:
		tcdc = "Tân Sửu"
		break
	case 42:
		tcdc = "Nhâm Dần"
		break
	case 43:
		tcdc = "Quý Mão"
		break
	case 44:
		tcdc = "Giáp Thìn"
		break
	case 45:
		tcdc = "Ất Tỵ"
		break
	case 46:
		tcdc = "Bính Ngọ"
		break
	case 47:
		tcdc = "Đinh Mùi"
		break
	case 48:
		tcdc = "Mậu Thân"
		break
	case 49:
		tcdc = "Kỷ Dậu"
		break
	case 50:
		tcdc = "Canh Tuất"
		break
	case 51:
		tcdc = "Tân Hợi"
		break
	case 52:
		tcdc = "Nhâm Tý"
		break
	case 53:
		tcdc = "Quý Sửu"
		break
	case 54:
		tcdc = "Giáp Dần"
		break
	case 55:
		tcdc = "Ất Mão"
		break
	case 56:
		tcdc = "Bính Thìn"
		break
	case 57:
		tcdc = "Đinh Tỵ"
		break
	case 58:
		tcdc = "Mậu Ngọ"
		break
	case 59:
		tcdc = "Kỷ Mùi"
		break
	}

	fmt.Printf(tcdc)
	return tcdc
}
