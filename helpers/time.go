package helpers

import (
	"time"
)

func GetTimeNow() time.Time {
	t := time.Now()
	loc, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	now := t.In(loc)
	return now
}

func ParseTime(date string) time.Time {
	timeLayout := "2006-01-02 15:04:05"
	loc, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	t, _ := time.ParseInLocation(timeLayout, date, loc)
	return t
}
