package repo

import (
	"context"
	"time"

	"gitlab.com/huynguyen.08.08.98/xemtuvi/config"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/services/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoTCTVRepo struct {
	c *mongo.Collection
}

func NewMongoTCTVRepo() domain.ITraCuuTuViRepo {
	c := config.GetConfig()
	mongo := database.NewMongoService()

	return &mongoTCTVRepo{
		c: mongo.Client.Database(c.Mongo.Database).Collection("tuvi2021"),
	}
}

func (m *mongoTCTVRepo) FindOne(ctx context.Context, conditions map[string]interface{}, options ...*options.FindOptions) (*domain.TraCuuTuVi, error) {
	var tctv *domain.TraCuuTuVi
	if err := m.c.FindOne(ctx, conditions).Decode(&tctv); err != nil {
		return nil, err
	}

	return tctv, nil
}

func (m *mongoTCTVRepo) Insert(ctx context.Context, tctv *domain.TraCuuTuVi) (*domain.TraCuuTuVi, error) {
	now := time.Now()
	tctv.CreatedAt = now
	tctv.UpdatedAt = now

	result, err := m.c.InsertOne(ctx, tctv)
	if err != nil {
		return nil, err
	}

	tctv.ID = result.InsertedID.(primitive.ObjectID)
	return tctv, nil
}
