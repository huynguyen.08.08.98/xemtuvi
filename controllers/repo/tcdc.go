package repo

import (
	"context"
	"time"

	"gitlab.com/huynguyen.08.08.98/xemtuvi/config"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/services/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoTCDCRepo struct {
	c *mongo.Collection
}

func NewMongoTCDCRepo() domain.IThienCanDiaChiRepo {
	c := config.GetConfig()
	mongo := database.NewMongoService()

	return &mongoTCDCRepo{
		c: mongo.Client.Database(c.Mongo.Database).Collection("thiencan_diachi"),
	}
}

func (m *mongoTCDCRepo) FindOne(ctx context.Context, conditions map[string]interface{}, options ...*options.FindOptions) (*domain.ThienCanDiaChi, error) {
	var tcdc *domain.ThienCanDiaChi
	if err := m.c.FindOne(ctx, conditions).Decode(&tcdc); err != nil {
		return nil, err
	}

	return tcdc, nil
}

func (m *mongoTCDCRepo) Insert(ctx context.Context, tcdc *domain.ThienCanDiaChi) (*domain.ThienCanDiaChi, error) {
	now := time.Now()
	tcdc.CreatedAt = now
	tcdc.UpdatedAt = now

	result, err := m.c.InsertOne(ctx, tcdc)
	if err != nil {
		return nil, err
	}

	tcdc.ID = result.InsertedID.(primitive.ObjectID)
	return tcdc, nil
}
