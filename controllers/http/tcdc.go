package http

import (
	"encoding/json"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/helpers"
)

type responseErr struct {
	Error domain.Error `json:"error"`
}

type responeData struct {
	Data interface{} `json:"data"`
}

type tcdcHTTPHandler struct {
	tcdcUsecase domain.IThienCanDiaChiUsecase
	tctvUsecase domain.ITraCuuTuViUsecase
}

func NewTCDCHTTPHandler(e *echo.Echo, tcdcu domain.IThienCanDiaChiUsecase, tctv domain.ITraCuuTuViUsecase) {
	handler := &tcdcHTTPHandler{
		tcdcUsecase: tcdcu,
		tctvUsecase: tctv,
	}
	e.POST("new-data.json", handler.CreateData)
	e.POST("tuvitrondoi.json", handler.GetThienCanDiaChi)
	e.POST("xemtuvi-2021.json", handler.GetData2021)

	e.GET("import-data.json", handler.ImportData)
}

func (th *tcdcHTTPHandler) CreateData(c echo.Context) error {
	ctx := c.Request().Context()
	name := c.FormValue("name")
	body := c.FormValue("body")
	sex := c.FormValue("sex")

	tcdc := new(domain.ThienCanDiaChi)
	tcdc.Name = name
	tcdc.Body = body
	tcdc.Sex = sex

	result, err := th.tcdcUsecase.CreateData(ctx, tcdc)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &responseErr{
			Error: domain.Error{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
				Type:    "TCDCException",
			},
		})
	}
	return c.JSON(http.StatusOK, &responeData{Data: result})

}

func (th *tcdcHTTPHandler) GetData2021(c echo.Context) error {
	ctx := c.Request().Context()

	m := echo.Map{}
	if err := c.Bind(&m); err != nil {
		return err
	}

	jsonString, _ := json.Marshal(m)
	// convert json to struct
	reqTCTV := new(domain.RequestTCTV)
	json.Unmarshal(jsonString, &reqTCTV)

	result, err := th.tctvUsecase.GetTCTV(ctx, reqTCTV)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &responseErr{
			Error: domain.Error{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
				Type:    "TCDCException",
			},
		})
	}
	return c.JSON(http.StatusOK, &responeData{Data: result})
}

func (th *tcdcHTTPHandler) GetThienCanDiaChi(c echo.Context) error {
	ctx := c.Request().Context()

	m := echo.Map{}
	if err := c.Bind(&m); err != nil {
		return err
	}

	jsonString, _ := json.Marshal(m)
	// convert json to struct
	reqTCDC := new(domain.RequestTCDC)
	json.Unmarshal(jsonString, &reqTCDC)

	result, err := th.tcdcUsecase.GetThienCanDiaChi(ctx, reqTCDC)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &responseErr{
			Error: domain.Error{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
				Type:    "TCDCException",
			},
		})
	}
	return c.JSON(http.StatusOK, &responeData{Data: result})

}

func (th *tcdcHTTPHandler) ImportData(c echo.Context) error {
	links := helpers.ReadSiteMap("sitemap.xml")
	helpers.VisitLink(links, th.tctvUsecase)
	return c.JSON(http.StatusOK, "ok")

}
