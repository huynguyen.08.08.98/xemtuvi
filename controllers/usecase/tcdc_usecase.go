package usecase

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/helpers"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type tcdcUsecase struct {
	tcdcRepo       domain.IThienCanDiaChiRepo
	contextTimeout time.Duration
}

func NewThienCanDiaChiUsecase(tcdcr domain.IThienCanDiaChiRepo, timeOut time.Duration) domain.IThienCanDiaChiUsecase {
	return &tcdcUsecase{
		tcdcRepo:       tcdcr,
		contextTimeout: timeOut,
	}
}

func (tcdcu *tcdcUsecase) CreateData(c context.Context, tcdc *domain.ThienCanDiaChi) (*domain.ThienCanDiaChi, error) {
	ctx, cancel := context.WithTimeout(c, tcdcu.contextTimeout)
	defer cancel()

	tcdc, err := tcdcu.tcdcRepo.Insert(ctx, tcdc)

	if err != nil {
		logrus.WithField("Data", fmt.Sprintf("%v", tcdc)).WithError(err).Error("Insert data failed")
		return nil, err
	}

	return tcdc, nil
}

func (tcdcu *tcdcUsecase) GetThienCanDiaChi(c context.Context, request *domain.RequestTCDC) (*domain.ThienCanDiaChi, error) {
	ctx, cancel := context.WithTimeout(c, tcdcu.contextTimeout)
	defer cancel()

	filter := bson.M{}
	year, err := strconv.Atoi(request.Year)
	if err != nil {
		return nil, err
	}

	tcdc := helpers.GetTCDCByYear(year)
	filter["name"] = tcdc
	filter["sex"] = request.Sex

	options := options.Find()
	result, err := tcdcu.tcdcRepo.FindOne(ctx, filter, options)

	if err != nil {
		logrus.WithField("Data", fmt.Sprintf("%v", filter)).WithError(err).Error("Data not found")
		return nil, err
	}

	return result, nil
}
