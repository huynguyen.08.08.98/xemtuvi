BINARY=build/api-server
.PHONY: start logs-app ssh-app logs

vendor:
	go mod vendor

build: vendor
	go build -o ${BINARY}

start:
	docker-compose up -d
stop:
	docker-compose stop
logs:
	docker logs -f xemtuvi-api
ssh-app:
	docker exec -it xemtuvi-api bash