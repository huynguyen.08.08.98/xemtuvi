package domain

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ThienCanDiaChi struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"name" bson:"name"`
	Sex       string             `json:"sex" bson:"sex"`
	Body      string             `json:"body" bson:"body"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
}

type RequestTCDC struct {
	Year string `json:"year"`
	Sex  string `json:"sex"`
}

type IThienCanDiaChiUsecase interface {
	CreateData(context.Context, *ThienCanDiaChi) (*ThienCanDiaChi, error)
	GetThienCanDiaChi(context.Context, *RequestTCDC) (*ThienCanDiaChi, error)
}

type IThienCanDiaChiRepo interface {
	Insert(context.Context, *ThienCanDiaChi) (*ThienCanDiaChi, error)
	FindOne(context.Context, map[string]interface{}, ...*options.FindOptions) (*ThienCanDiaChi, error)
}
