package domain

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type TraCuuTuVi struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Sex       string             `json:"sex" bson:"sex"`
	Body      string             `json:"body" bson:"body"`
	Name      string             `json:"name" bson:"name"`
	Title     string             `json:"title" bson:"title"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
}

type RequestTCTV struct {
	Year string `json:"year"`
	Sex  string `json:"sex"`
}

type ITraCuuTuViUsecase interface {
	CreateData(context.Context, *TraCuuTuVi) (*TraCuuTuVi, error)
	GetTCTV(context.Context, *RequestTCTV) (*TraCuuTuVi, error)
}

type ITraCuuTuViRepo interface {
	Insert(context.Context, *TraCuuTuVi) (*TraCuuTuVi, error)
	FindOne(context.Context, map[string]interface{}, ...*options.FindOptions) (*TraCuuTuVi, error)
}
