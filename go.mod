module gitlab.com/huynguyen.08.08.98/xemtuvi

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/antchfx/htmlquery v1.2.3 // indirect
	github.com/antchfx/xmlquery v1.3.3 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.9.0
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/gosimple/slug v1.9.0
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.17 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.3.2
	github.com/temoto/robotstxt v1.1.1 // indirect
	go.mongodb.org/mongo-driver v1.4.5
	google.golang.org/appengine v1.6.7 // indirect
)
